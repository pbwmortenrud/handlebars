const helper = {
    if_cond: (v1, op, v2, options) => {
        switch (op) { 
            case '!=':  //og mange andre
                return (v1 != v2) ? options.fn(this) : options.inverse(this);
            default:
                return options.inverse(this);
        }
    }
}

module.exports = helper;