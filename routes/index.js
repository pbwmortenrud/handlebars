var express = require("express");
var router = express.Router();


/* GET home page. */
router.get("/", function (req, res, next) {
  res.render("index", { title: "Express View Engines", subject: "Handlebars"});
});

/* GET about page. */
router.get("/about", function (req, res, next) {
  res.render("about", { title: "About"});
});

//Get continents.js
const continents = require("../public/javascripts/continents");
/* GET continents page. */
router.get("/continents", function (req, res, next) {
  res.render("continents", { title: "Continents", subject: "Handlebars", data: continents});
});

/* GET forms page. */
router.get("/forms", function (req, res, next) {
  res.render("forms", { title: "Forms"});
});
router.post("/signup", (req, res) => {
  let na = req.body.nameInput;
  let em = req.body.emailInput;
  res.render('forms', {title: 'SignUp', email:em, name:na});
  console.log(req.params);
  console.log(req.body);
  console.log(req.url);
  console.log(req.query); 
})

module.exports = router;
