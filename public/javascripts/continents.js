/* nml: continents of the world */
"use strict";

module.exports =  {
    "Africa": "Africa",
    "Antarctica": "Antarctica",
    "Asia": "Asia",
    "Europe": "Europe",
    "North_America": "North America",
    "Oceania": "Oceania",
    "South_America": "South America"
}